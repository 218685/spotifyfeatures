import tensorflow as tf
from tensorflow.contrib import rnn
import os
from random import shuffle
import pydub
from pydub.exceptions import CouldntDecodeError
import scipy.signal
import scipy.io.wavfile
import pickle
from time import sleep

with tf.device("/gpu:0"):

    # Parameters:
    num_epochs = 500
    num_hidden = 500
    display_step = 100
    batch_size = 5
    dataset = os.listdir('./res/')
    dataset_length = len(dataset)
    train_length = int(dataset_length * 0.85)
    shuffle(dataset)
    train_data = dataset[:train_length]
    validate_data = dataset[train_length:]
    num_inputs = 2001
    timesteps = 378

    # Input for graph:
    x_placeholder = tf.placeholder(tf.float32, [timesteps, num_inputs])
    y_placeholder = tf.placeholder(tf.float32)

    # Weights:
    weights = tf.Variable(tf.random_normal([num_hidden, 1]))
    biases = tf.Variable(tf.random_normal([1]))

    def get_data(name, dct):
        with open('./res/{}'.format(name), 'rb') as file:
            mp3 = pydub.AudioSegment.from_mp3(file)
            mp3.export('./temp/{}.wav'.format(name.rstrip('.mp3')), format='wav')
        rate, audData = scipy.io.wavfile.read('./temp/{}.wav'.format(name.rstrip('.mp3')))
        channel1 = audData[:, 0]
        window = scipy.signal.get_window('triang', 4000)
        frequencies, times, spectrogram = scipy.signal.spectrogram(channel1, rate,
                                                                   return_onesided=True, window=window)
        loudness = dct.get(name, -10)
        try:
            os.remove('./temp/{}.wav'.format(name.rstrip('.mp3')))
        except PermissionError:
            sleep(1)
            os.remove('./temp/{}.wav'.format(name.rstrip('.mp3')))
        return spectrogram, loudness

    def RNN(x, weights, biases):
        lstm_cell = rnn.BasicLSTMCell(num_hidden)
        outputs, states = rnn.static_rnn(lstm_cell, [x], dtype=tf.float32)
        pred = tf.matmul(outputs[-1], weights) + biases
        return pred

    prediction = tf.transpose(-RNN(x_placeholder, weights, biases))[0]
    loss = abs(tf.reduce_sum(prediction)/tf.to_float(tf.size(prediction)) - y_placeholder)
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
    train_op = optimizer.minimize(loss)

    init = tf.global_variables_initializer()

saver = tf.train.Saver()

with tf.Session() as sess:
    sess.run(init)

    with open('track_loudness.pickle', 'rb') as file:
        track_loudness = pickle.load(file)
    try:
        saver.restore(sess, './model/model.ckpt')
    except:
        pass

    for epoch in range(num_epochs):
        print("Starting epoch {}".format(epoch))
        shuffle(train_data)

        for track in range(len(train_data)):
            try:
                x, y = get_data(train_data[track], track_loudness)

            except pydub.exceptions.CouldntDecodeError:
                print("Couldnt decode track", train_data[track])
                continue
            try:
                sess.run(train_op, feed_dict={x_placeholder: x.transpose(),
                                              y_placeholder: y})
            except ValueError as e:
                print("Passed", e)
                pass
            if track % display_step == 0:
                print(track, train_data[track], y)

        validation_list = []
        for track in validate_data:
            try:
                x, y = get_data(track, track_loudness)
            except pydub.exceptions.CouldntDecodeError:
                print("Couldnt decode track", track)
                continue
            try:
                pred = sess.run(prediction, feed_dict={x_placeholder: x.transpose(), y_placeholder: y})
                loss_ = abs(sum(pred)/len(pred) - y)
                validation_list.append(loss_)
            except ValueError as e:
                print("Passed", e)
                pass

        print("Mean loss: ", sum(validation_list)/len(validation_list))
        print("Max loss: ", max(validation_list))
        print("Min loss: ", min(validation_list))

        if epoch % 3 == 1:
            saver.save(sess, './model/model.ckpt')
    print("Optimization finished!")


