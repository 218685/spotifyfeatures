import sys
import spotipy
import spotipy.oauth2 as oauth
import spotipy.util as util
import requests
from time import sleep
import os


if __name__ == '__main__':
    username = 'username'
    client_id = 'client_id'
    client_secret = 'client_secret'

    # token = util.prompt_for_user_token(username, client_id=client_id, client_secret=client_secret,
    #                                    redirect_uri='http://localhost:8081')
    auth = oauth.SpotifyClientCredentials(client_id, client_secret)
    token = auth.get_access_token()
    if token:
        sp = spotipy.Spotify(auth=token)
        artists = ['Adele', 'Michael Jackson', 'Justice', 'Wlodi', 'Fatboy Slim', 'Led Zeppelin', 'Skepta',
                   'Black Sabbath', 'Dire Straits', 'OM', 'Graveyard', 'All them witches', 'Mgła', 'Aesop Rock',
                   'MF DOOM', 'Brother Ali', 'Flying Lotus', 'Daft punk', 'A tribe called quest',
                   'Digitalism', 'Kraftwerk', 'Aphex Twin']
        artists_ids = {}
        i = 0
        for artist in artists:
            result = sp.search(artist, type='artist')
            i += 1
            artists_ids[artist] = result['artists']['items'][0]['id']
            rel = sp.artist_related_artists(artists_ids[artist])
            for rel_artist in rel['artists']:
                i += 1
                artists_ids[rel_artist['name']] = rel_artist['id']
                if not i % 25:
                    sleep(1)
            if not i % 25:
                sleep(1)

        i = 0
        already_downloaded = os.listdir('./res')
        tracks_links = {}
        track_loudness = {}
        for artist, id_ in artists_ids.items():
            tracks = sp.artist_top_tracks(id_)
            i += 1
            for track in tracks['tracks']:
                name = artist + '-' + track['name'].replace('/', '').replace('"', '').replace('?', '')
                tracks_links[name] = track['preview_url']
                i += 1
                if not i % 15:
                    sleep(1)
                    print(len(track_loudness))
                    print(track_loudness)
                try:
                    track_features = sp.audio_features([track['id']])
                except:
                    sleep(2)
                    track_features = sp.audio_features([track['id']])
                track_loudness[name] = track_features[0]['loudness']

        for name, url in tracks_links.items():
            if url and name+'.mp3' not in already_downloaded:
                print("Getting {}".format(name))
                resp = requests.get(url, stream=True)
                i += 1
                try:
                    with open('./res/{}.mp3'.format(name), 'wb') as f:
                        resp.raw.decode_content = True
                        f.write(resp.content)
                except OSError:
                    pass
                if not i % 25:
                    sleep(1)

    else:
        print("Can't get token for", username)
