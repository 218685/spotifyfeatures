import tensorflow as tf
from tensorflow.contrib import rnn
import os
import pydub
from pydub.exceptions import CouldntDecodeError
import scipy.signal
import scipy.io.wavfile
import pickle

with tf.device("/gpu:0"):

    # Parameters:
    num_hidden = 500
    display_step = 100
    batch_size = 5
    dataset = os.listdir('./res2/')
    dataset_length = len(dataset)
    num_inputs = 2001
    timesteps = 378

    # Input for graph:
    x_placeholder = tf.placeholder(tf.float32, [timesteps, num_inputs])
    y_placeholder = tf.placeholder(tf.float32)

    # Weights:
    weights = tf.Variable(tf.random_normal([num_hidden, 1]))
    biases = tf.Variable(tf.random_normal([1]))

    def get_data(name, dct):
        with open('./res2/{}'.format(name), 'rb') as file:
            mp3 = pydub.AudioSegment.from_mp3(file)
            mp3.export('./temp/{}.wav'.format(name), format='wav')
        rate, audData = scipy.io.wavfile.read('./temp/{}.wav'.format(name))
        channel1 = audData[:, 0]
        window = scipy.signal.get_window('triang', 4000)
        frequencies, times, spectrogram = scipy.signal.spectrogram(channel1, rate,
                                                                   return_onesided=True, window=window)
        loudness = dct.get(name, -8)
        os.remove('./temp/{}.wav'.format(name))
        return spectrogram, loudness

    def RNN(x, weights, biases):
        lstm_cell = rnn.BasicLSTMCell(num_hidden)
        outputs, states = rnn.static_rnn(lstm_cell, [x], dtype=tf.float32)
        pred = tf.matmul(outputs[-1], weights) + biases
        return pred

    prediction = - RNN(x_placeholder, weights, biases)

    loss = abs(prediction - y_placeholder)

    init = tf.global_variables_initializer()

saver = tf.train.Saver()

with tf.Session() as sess:
    sess.run(init)

    with open('track_loudness.pickle', 'rb') as f:
        track_loudness = pickle.load(f)
    try:
        saver.restore(sess, './model/model.ckpt')
    except:
        pass

    i = 0
    test_list = []
    for track in dataset:
        try:
            x, y = get_data(track, track_loudness)

        except pydub.exceptions.CouldntDecodeError:
            print("Couldnt decode track", track)
            continue

        try:
            pred = sess.run(prediction, feed_dict={x_placeholder: x.transpose(), y_placeholder: y}).transpose()[0]
            loss_ = abs(sum(pred)/len(pred) - y)
            print(track, "Predicted value:", y+loss_, "Actual value:", y)
            test_list.append(loss_)
        except ValueError as e:
            print("Passed", e)
            pass

    print("Mean loss: ", sum(test_list)/len(test_list))
    print("Max loss: ", max(test_list))
    print("Min loss: ", min(test_list))


